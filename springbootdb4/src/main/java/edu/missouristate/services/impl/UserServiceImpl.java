package edu.missouristate.services.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import edu.missouristate.dao.UserRepository;
import edu.missouristate.model.User;
import edu.missouristate.services.UserService;
import edu.missouristate.util.Helper;

@Service("userService")
public class UserServiceImpl implements UserService {
    
    @Autowired
    UserRepository userRepo;

	@Override
	public List<User> getUsers() {
		return userRepo.getUsers();
	}

	@Override
	@Transactional
	public void addUser(Model model, User user) {
		userRepo.addUser(user);
	}

	@Override
	public String prepareAddUser(Model model) {
		// Create a new instance of User
		User user = new User();
		
		// Add the empty user and title to the model
		model.addAttribute("command", user);
		model.addAttribute("title", "Add User");
		return "addEditUser";
	}

	@Override
	public String prepareEditUser(HttpSession session, Model model, Integer id) {
		// Get the user by ID
		User user = getUserById(id);
		
		if (user != null) {
			model.addAttribute("command", user);
			model.addAttribute("title", "Edit User");
			return "addEditUser";
		} else {
			// Provide error message to the user and prepare Add User
			Helper.addSessionMessage(session, "Sorry, user with ID = " + id + " not found.");
			return "redirect:/users";
		}
	}

	@Override
	public User getUserById(Integer id) {
		return userRepo.getUserById(id);
	}

	@Override
	public void updateUser(Model model, User user) {
		userRepo.updateUser(user);
	}

	@Override
	public int deleteUser(Model model, int id) {
		return userRepo.deleteUser(id);
	}
   
}
