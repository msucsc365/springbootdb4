package edu.missouristate.services;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import edu.missouristate.model.User;

public interface UserService {
	public List<User> getUsers();
	public User getUserById(Integer id);
	public void addUser(Model model, User user);
	public String prepareAddUser(Model model);
	public String prepareEditUser(HttpSession session, Model model, Integer id);
	public void updateUser(Model model, User user);
	public int deleteUser(Model model, int id);
}
