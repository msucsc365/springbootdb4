package edu.missouristate.util;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import edu.missouristate.model.User;

public class MSU {

	public static final BeanPropertyRowMapper<User> USER_BPRM = new BeanPropertyRowMapper<User>(User.class);
	
}
