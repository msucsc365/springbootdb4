package edu.missouristate.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdb4Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootdb4Application.class, args);
	}

}
