package edu.missouristate.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import edu.missouristate.model.User;
import edu.missouristate.services.UserService;
import edu.missouristate.util.Helper;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping("/users")
	public String getUserTable(Model model) {
		List<User> userList = userService.getUsers();
		model.addAttribute("userList", userList);
		return "users";
	}
	
	@GetMapping("/users/addEditUser")
	public String getAddEditUser(HttpSession session, Model model, String id) {
		// Default the next page to addEditUser
		String page = "addEditUser";
		
		// If the ID is an integer, we need to load the "edit user" page
		// Otherwise, we need to load the "add user" page
		if (Helper.isInteger(id)) {
			page = userService.prepareEditUser(session, model, Integer.parseInt(id));
		} else {
			page = userService.prepareAddUser(model);
		}
		
		// Return the Add Edit JSP or User's Page
		return page;
	}
	
	@PostMapping("/users/addEditUser")
	public String postAddEditUser(HttpSession session, Model model, User user) {
		
		if (user != null && user.getId() != null) {
			userService.updateUser(model, user);
		} else {
			Helper.addSessionMessage(session, "Sorry, could not find user");
		}
		
		return "redirect:/users";
	}
	
	@GetMapping("/users/deleteUser")
	public String getDeleteUser(HttpSession session, Model model, String id) {
		
		if (Helper.isInteger(id)) {
			int deleteCount = userService.deleteUser(model, Integer.parseInt(id));
			Helper.addSessionMessage(session, "Records Deleted: " + deleteCount);
		} else {
			String idString = ((id == null) ? "''" : ("'"+id+"'"));
			Helper.addSessionMessage(session, "Sorry, could not find user with id = " + idString);
		}
		
		return "redirect:/users";
	}
	
}
