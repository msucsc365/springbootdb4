<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<%-- Title --%>
				<h1>Example Page</h1>
				
				<%-- User Message --%>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				
				<%-- Users --%>
				<div class="row mt-3">
					<div class="col-12">
						<%-- No Records Found --%>
						<c:if test="${empty userList}">
							No Records Found
						</c:if>
						
						<%-- Records Found --%>
						<c:if test="${not empty userList}">
							<c:forEach var="user" items="${userList}">
								<a class="no-decor" href="<%=request.getContextPath() %>/users/addEditUser?id=${user.id}">
									<i id="edit_${user.id}" class="fas fa-pencil-alt dark-yellow" aria-hidden="true"></i>
								</a>
								<a class="no-decor" href="<%=request.getContextPath() %>/users/deleteUser?id=${user.id}">
									<i id="delete_${user.id}" class="fa fa-times red ml-1" aria-hidden="true"></i>
								</a>
							    <span class="ml-1">${user}</span>
							    <br/> 
							</c:forEach>
						</c:if>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</body>
</html>